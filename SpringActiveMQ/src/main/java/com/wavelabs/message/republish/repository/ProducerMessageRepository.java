package com.wavelabs.message.republish.repository;


import org.springframework.data.repository.CrudRepository;

import com.wavelabs.message.republish.model.ProducerMessage;

public interface ProducerMessageRepository extends CrudRepository<ProducerMessage, Integer>
{

}
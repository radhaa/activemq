package com.wavelabs.message.republish.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.message.republish.model.ProducerMessage;
import com.wavelabs.message.republish.service.MessageProducerService;
import com.wavelabs.message.republish.service.ProducerMessageService;
@RestController
@RequestMapping("/message")
public class ProducerMessageController 
{
	@Autowired
	MessageProducerService producer;
	@Autowired
	ProducerMessageService messageService;

	@RequestMapping(value="/post",method=RequestMethod.POST)
	public void messageSend(@RequestBody ProducerMessage producerMessage)

	{
		System.out.println(producerMessage);
		messageService.messageSend(producerMessage);

	}

}
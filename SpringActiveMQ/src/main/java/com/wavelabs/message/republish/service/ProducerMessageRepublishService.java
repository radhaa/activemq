package com.wavelabs.message.republish.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.wavelabs.message.republish.model.ProducerMessage;
import com.wavelabs.message.republish.repository.ProducerMessageRepository;
@Service
public class ProducerMessageRepublishService {
	@Autowired
	ProducerMessageRepository repo;
	@Autowired
	MessageProducerService producer;
	@Scheduled(fixedDelay = 120000)
	public void resend()
		{
		try{
			Iterable<ProducerMessage> it=repo.findAll();
			for(ProducerMessage message:it) 
			{
				 producer.send(message);	
				 
				 repo.delete(message);
			} 
		}
		catch(Exception e)
		{
			System.out.println("no messages to send");
		}
	}

}

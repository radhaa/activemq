package com.wavelabs.message.republish.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import com.wavelabs.message.republish.model.ProducerMessage;


@Service
public class MessageProducerService {
	@Autowired
	JmsTemplate jmsTemplate;
	
	@Value("${jsa.activemq.queue}")
	String queue;
	
	public void send(ProducerMessage message){
		jmsTemplate.convertAndSend(queue, message);
	}
}
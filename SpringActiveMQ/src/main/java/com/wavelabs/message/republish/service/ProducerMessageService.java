package com.wavelabs.message.republish.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.message.republish.model.ProducerMessage;
import com.wavelabs.message.republish.repository.ProducerMessageRepository;

@Service
public class ProducerMessageService {
	@Autowired
	MessageProducerService producer;
	@Autowired
	ProducerMessageRepository repo;

	public void messageSend(ProducerMessage message) {
		try {
			System.out.println("in try");
			System.out.println(message);
			producer.send(message);
		} catch (Exception e) {
			e.printStackTrace();
			saveMessage(message);
		}
	}

	public ProducerMessageRepository saveMessage(ProducerMessage message) {
		try {
			repo.save(message);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("cont");

		}
		return repo;
	}

}

package com.wavelabs.message.republish;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class MessageRepublishApplication {

	public static void main(String[] args) {
		SpringApplication.run(MessageRepublishApplication.class, args);
	}
}



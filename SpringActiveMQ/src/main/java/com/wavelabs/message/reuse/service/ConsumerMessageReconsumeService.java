package com.wavelabs.message.reuse.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.wavelabs.message.reuse.model.ConsumerMessage;
import com.wavelabs.message.reuse.repository.ConsumerMessageRepository;
@Service
public class ConsumerMessageReconsumeService {
	@Autowired
	ConsumerMessageRepository repo;
	@Autowired
	MessageConsumer consumer;
	@Autowired
	MessageConsumerService mcs;
	@Scheduled(fixedDelay = 120000)
	public void resend()
	{
		try{
			Iterable<ConsumerMessage> it=repo.findAll();
			for(ConsumerMessage message: it) 
			{
				int count=mcs.process(message);
				if(count==3) {
					System.out.println(message);
					repo.delete(message);
				}
			} 
		}
		catch(Exception e)
		{
			System.out.println("no messages to recieve");
		}
	}

}

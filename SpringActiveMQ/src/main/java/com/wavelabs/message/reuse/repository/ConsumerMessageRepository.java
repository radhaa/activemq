package com.wavelabs.message.reuse.repository;

import org.springframework.data.repository.CrudRepository;

import com.wavelabs.message.reuse.model.ConsumerMessage;

public interface ConsumerMessageRepository extends CrudRepository<ConsumerMessage, Integer>{

}

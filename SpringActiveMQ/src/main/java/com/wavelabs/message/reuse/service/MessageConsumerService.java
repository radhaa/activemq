package com.wavelabs.message.reuse.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.wavelabs.message.reuse.model.ConsumerMessage;
import com.wavelabs.message.reuse.model.ProducerMessage;
import com.wavelabs.message.reuse.repository.ConsumerMessageRepository;
@Component
@Service
public class MessageConsumerService 
{
	@Autowired
	ConsumerMessageRepository repo;
	public ConsumerMessage createConsumerMessage(ProducerMessage message) {
		Date date= new Date();
		ConsumerMessage cm=new ConsumerMessage();
		cm.setId(message.getId());
		cm.setMessage(message.getMessage());
		cm.setDate(date);
		return cm;
	}

	public ProducerMessage createProducerMessage(ConsumerMessage message) {
		ProducerMessage cm=new ProducerMessage();
		cm.setId(message.getId());
		cm.setMessage(message.getMessage());
		return cm;
	}

	public synchronized int process(ConsumerMessage message)
	{ int flag=0;
		if(message.getId()%2==0)
		{
		flag=3;
		}
		else {
			try {
				Thread.sleep(60000);
				flag=3;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return flag;
	}

	public ConsumerMessageRepository saveMessage(ConsumerMessage message) {
		try {
			repo.save(message);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("cont");
		}
		return repo;
	}
}

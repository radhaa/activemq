package com.wavelabs.message.reuse.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import com.wavelabs.message.reuse.model.ConsumerMessage;
import com.wavelabs.message.reuse.model.ProducerMessage;


@Service
public class MessageConsumer {
	@Autowired
	MessageConsumerService mcs;
	@JmsListener(destination = "${jsa.activemq.queue}", containerFactory="jsaFactory")
	public ConsumerMessage receive(ProducerMessage message){
		ConsumerMessage consumerMessage=null;
		try {
			consumerMessage=mcs.createConsumerMessage(message);
			mcs.saveMessage(consumerMessage);

		} catch (Exception e) {
			mcs.saveMessage(consumerMessage);
		}
		return null;
	}


}

package com.wavelabs.activemq.xml.message.subscriber;

import java.io.StringReader;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

@SpringBootApplication
public class XmlMessageConsumer1Application {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(XmlMessageConsumer1Application.class, args);
		Connection connection = null;
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(

				"tcp://localhost:61616");

		connection = connectionFactory.createConnection();

		Session session = connection.createSession(false,

				Session.AUTO_ACKNOWLEDGE);

		Queue queue = session.createQueue("xml-Queue");
		MessageConsumer consumer = session.createConsumer(queue);

		connection.start();

		TextMessage textMsg = (TextMessage) consumer.receive();

		String xml = textMsg.getText();

		System.out.println("Received: '" + xml + "'");

		Document receivedDoc = getXmlAsDOMDocument(xml);

		Node messagesNode = receivedDoc.getFirstChild();

		NodeList nodeList = messagesNode.getChildNodes();

		int msgCount = 0;

		for (int i = 0; i < nodeList.getLength(); i++) {

			Node childNode = nodeList.item(i);

			if (childNode.getNodeName().equals("message")) {

				msgCount++;

			}

		}

		System.out.println("message: " + msgCount);


	}
	public static Document getXmlAsDOMDocument(String xmlString) throws Exception {

		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory

				.newInstance();

		DocumentBuilder documentBuilder = documentBuilderFactory

				.newDocumentBuilder();

		return documentBuilder.parse(

				new InputSource(new StringReader(xmlString)));

	}

}

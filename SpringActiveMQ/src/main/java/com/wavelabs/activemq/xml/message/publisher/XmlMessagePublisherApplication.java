package com.wavelabs.activemq.xml.message.publisher;

import java.io.IOException;
import java.io.StringReader;

import java.io.StringWriter;

import java.net.URISyntaxException;

 

import javax.jms.Connection;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;

import javax.jms.MessageConsumer;

import javax.jms.MessageProducer;

import javax.jms.Queue;

import javax.jms.Session;


import javax.xml.parsers.DocumentBuilder;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;

import javax.xml.transform.dom.DOMSource;

import javax.xml.transform.stream.StreamResult;

 

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.w3c.dom.Document;

import org.xml.sax.SAXException;


@SpringBootApplication
public class XmlMessagePublisherApplication {

	public static void main(String[] args) throws JMSException, ParserConfigurationException, SAXException, IOException, TransformerException  {
		Connection connection = null;
		SpringApplication.run(XmlMessagePublisherApplication.class, args);
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(

				"tcp://localhost:61616");

		connection = connectionFactory.createConnection();

		Session session = connection.createSession(false,

				Session.AUTO_ACKNOWLEDGE);

		Queue queue = session.createQueue("xml-Queue");

		MessageProducer producer = session.createProducer(queue);

		Document doc = parseXml();

		String xmlPayload = getXmlAsString(doc);

		Message msg = session.createTextMessage(xmlPayload);

		System.out.println("Sending text '" + xmlPayload + "'");

		producer.send(msg);
	}

	private static String getXmlAsString(Document doc) throws TransformerException  {
		TransformerFactory tf = TransformerFactory.newInstance();

		Transformer transformer = tf.newTransformer();

		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

		StringWriter writer = new StringWriter();

		transformer

		.transform(new DOMSource(doc), new StreamResult(writer));

		String output = writer.getBuffer().toString().replaceAll("\n|\r", "");

		return output;

	}

	private static Document parseXml() throws ParserConfigurationException, SAXException, IOException  {
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

		DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

		return documentBuilder.parse(XmlMessagePublisherApplication.class.getResourceAsStream("/message.xml"));

	}
}

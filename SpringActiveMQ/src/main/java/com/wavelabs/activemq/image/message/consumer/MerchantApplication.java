package com.wavelabs.activemq.image.message.consumer;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Base64;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class MerchantApplication {

	public static void main(String[] args) throws JMSException {
		SpringApplication.run(MerchantApplication.class, args);

		Connection connection = null;
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");
		connection = connectionFactory.createConnection();
		Session session = connection.createSession(false,Session.AUTO_ACKNOWLEDGE);
		Queue queue = session.createQueue("test-queue");

		MessageConsumer consumer = session.createConsumer(queue);
		connection.start();
		TextMessage textMsg = (TextMessage) consumer.receive();
		String img = textMsg.getText();

		decoder(img,"decoder.jpg" );

	}

	public static void decoder(String base64Image, String pathFile) {
		try (FileOutputStream imageOutFile = new FileOutputStream(pathFile)) {
			byte[] imageByteArray = Base64.getDecoder().decode(base64Image);
			imageOutFile.write(imageByteArray);
		} 
		catch (FileNotFoundException e) {
		} 
		
		catch (IOException ioe) {
		}

	}
}

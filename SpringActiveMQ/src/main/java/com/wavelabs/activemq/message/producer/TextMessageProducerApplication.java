package com.wavelabs.activemq.message.producer;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@SpringBootApplication
public class TextMessageProducerApplication {

	public static void main(String[] args) throws JMSException {
		SpringApplication.run(TextMessageProducerApplication.class, args);
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");
		Connection connection = connectionFactory.createConnection();
		Session session = connection.createSession(false,Session.AUTO_ACKNOWLEDGE);
		Queue queue = session.createQueue("msg-queue?consumer.exclusive=true");
		MessageProducer producer = session.createProducer(queue);
		Message msg = session.createTextMessage("hi");
	
		producer.send( msg);
	}
}

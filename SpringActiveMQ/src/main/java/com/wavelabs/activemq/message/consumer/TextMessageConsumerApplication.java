package com.wavelabs.activemq.message.consumer;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TextMessageConsumerApplication {

	public static void main(String[] args) throws JMSException {
		Connection connection = null;
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");
		connection = connectionFactory.createConnection();
		Session session = connection.createSession(false,Session.AUTO_ACKNOWLEDGE);
		Queue queue = session.createQueue("msg-queue?consumer.exclusive=true");
		MessageConsumer consumer = session.createConsumer(queue);
		connection.start();
		TextMessage textMsg = (TextMessage) consumer.receive();
		String text = textMsg.getText();
		System.out.println("Received: '" + text + "'");
	}
}

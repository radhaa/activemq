package com.wavelabs.activemq.json.message.annotation.jms.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.wavelabs.activemq.json.message.annotation.model.Company;

@Component
public class JmsProducer {
	@Autowired
	JmsTemplate jmsTemplate;
	
	@Value("${jsa.activemq.queue}")
	String queue;
	
	public void send(Company company){
		jmsTemplate.convertAndSend(queue, company);
	}
}
package com.wavelabs.activemq.json.message.annotation.jms.consumer;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.wavelabs.activemq.json.message.annotation.model.Company;

@Component
public class JmsConsumer {
	
	@JmsListener(destination = "${jsa.activemq.queue}", containerFactory="jsaFactory")
	public void receive(Company company){
		System.out.println("Recieved Message: " + company);
	}
}
